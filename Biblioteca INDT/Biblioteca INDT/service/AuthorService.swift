//
//  AuthorService.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class AuthorService {
    static func getAuthors(completion: @escaping (_ success: Bool?, _ message: String?, _ books: [Author]?) -> Void) {
        let URL = "https://bibliapp.herokuapp.com/api/authors"
        var parameters = Parameters()
        parameters["filter"] = Stringfy.stringify(json: ["order": "firstName ASC"])
        
        Alamofire.request(URL, method: .get, parameters: parameters).responseArray { (response: DataResponse<[Author]>) in
            if response.error != nil {
                completion(false, response.error?.localizedDescription, nil )
            } else {
                let authorsArray = response.result.value
                completion(true, nil, authorsArray)
            }
        }
    }
    
    static func addAuthor(firstName: String, lastName: String, completion: @escaping (_ success: Bool?, _ message: String?) -> Void) {
        let URL = "https://bibliapp.herokuapp.com/api/authors"
        var parameters = Parameters()
        parameters["firstName"] = firstName
        parameters["lastName"] = lastName
        
        Alamofire.request(URL, method: .put, parameters: parameters).response { (response) in
            if response.error != nil {
                completion(false, response.error?.localizedDescription)
            } else {
                completion(true, nil)
            }
        }
    }
    
    static func deleteAuthor(author: Author, completion: @escaping (_ success: Bool?, _ message: String?) -> Void) {
        let URL = "https://bibliapp.herokuapp.com/api/authors/\(author.id!)"
        Alamofire.request(URL, method: .delete).response { (response) in
            if response.error != nil {
                completion(false, response.error?.localizedDescription)
            } else {
                completion(true, nil)
            }
        }
    }
    
    static func editAuthor(id: Int, firstName: String, lastName: String, completion: @escaping (_ success: Bool?, _ message: String?) -> Void) {
        let URL = "https://bibliapp.herokuapp.com/api/authors/\(id)"
        var parameters = Parameters()
        parameters["firstName"] = firstName
        parameters["lastName"] = lastName
        
        Alamofire.request(URL, method: .put, parameters: parameters).response { (response) in
            if response.error != nil {
                completion(false, response.error?.localizedDescription)
            } else {
                completion(true, nil)
            }
        }
    }
}
