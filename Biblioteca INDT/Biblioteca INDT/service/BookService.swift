//
//  BookService.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class BookService {
    static func getBooks(completion: @escaping (_ success: Bool?, _ message: String?, _ books: [Book]?) -> Void) {
        let URL = "https://bibliapp.herokuapp.com/api/books"
        var parameters = Parameters()
        parameters["filter"] = Stringfy.stringify(json: ["order": "title ASC"])
        
        Alamofire.request(URL, method: .get, parameters: parameters).responseArray { (response: DataResponse<[Book]>) in
            if response.error != nil {
                completion(false, response.error?.localizedDescription, nil )
            } else {
                let booksArray = response.result.value
                completion(true, nil, booksArray)
            }
        }
    }
    
    static func addBook(title: String, isbn: String, authorId: Int, completion: @escaping (_ success: Bool?, _ message: String?) -> Void) {
        let URL = "https://bibliapp.herokuapp.com/api/books"
        var parameters = Parameters()
        parameters["title"] = title
        parameters["isbn"] = isbn
        parameters["authorId"] = authorId
        
        Alamofire.request(URL, method: .put, parameters: parameters).response { (response) in
            if response.error != nil {
                completion(false, response.error?.localizedDescription)
            } else {
                completion(true, nil)
            }
        }
    }
    
    static func deleteBook(book: Book, completion: @escaping (_ success: Bool?, _ message: String?) -> Void) {
        let URL = "https://bibliapp.herokuapp.com/api/books/\(book.id!)"
        Alamofire.request(URL, method: .delete).response { (response) in
            if response.error != nil {
                completion(false, response.error?.localizedDescription)
            } else {
                completion(true, nil)
            }
        }
    }
}
