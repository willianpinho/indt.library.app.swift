//
//  Book.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import ObjectMapper

class Book: Mappable {
    var title: String?
    var isbn: String?
    var id: Int?
    var authorId: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        isbn <- map["isbn"]
        id <- map["id"]
        authorId <- map["authorId"]
    }
}
