//
//  BookViewController.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class BookViewController: UIViewController {

    var book: Book!
    
    @IBOutlet weak var isbnLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavigationController()
        fillViewWithData()
    }

    func fillViewWithData() {
        idLabel.text = String(book.id!)
        isbnLabel.text = book.isbn
        titleLabel.text = book.title
        authorLabel.text = String(book.authorId!)
    }

    func customNavigationController() {
        self.title = "Detalhes"
    }
}
