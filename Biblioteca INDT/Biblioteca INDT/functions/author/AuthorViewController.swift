//
//  AuthorViewController.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class AuthorViewController: UIViewController {

    var author: Author!

    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavigationController()
        fillViewWithData()
        
    }
    
    func fillViewWithData() {
        firstNameLabel.text = author.firstName
        idLabel.text = String(author.id!)
        lastNameLabel.text = author.lastName
    }
    
    func customNavigationController() {
        self.title = "Detalhes"
        let edit = UIBarButtonItem(title: "Editar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(openEditAuthor))
        navigationItem.rightBarButtonItem = edit
    }
    
    @objc func openEditAuthor() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AuthorEditViewController") as? AuthorEditViewController
        vc?.author = author
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
