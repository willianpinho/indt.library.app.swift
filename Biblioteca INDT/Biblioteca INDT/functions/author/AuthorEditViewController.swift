//
//  AuthorEditViewController.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class AuthorEditViewController: UIViewController {

    var author: Author!
    
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavigationController()
        fillViewWithData()
    }
    
    func fillViewWithData() {
        firstNameTF.text = author.firstName
        lastNameTF.text = author.lastName
    }
    
    func customNavigationController() {
        let edit = UIBarButtonItem(title: "Editar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(editAuthor))
        navigationItem.rightBarButtonItem = edit
    }
    
    @objc func editAuthor() {
        AuthorService.editAuthor(id: author.id!, firstName: firstNameTF.text!, lastName: lastNameTF.text!) { (success, message) in
            if success! {
                self.navigationController?.popToRootViewController(animated: true)
            } else {
                let alert = UIAlertController(title: "Alert", message: message!, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
}
