//
//  FirstViewController.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class BooksViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    private var books: [Book] = []
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(BooksViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.gray
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        loadBooks()
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Livros"
        self.loader.hidesWhenStopped = true
        setup(tableView: tableView)
        customNavigationController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loader.startAnimating()
        loadBooks()
    }
    
    func customNavigationController() {
        let add = UIBarButtonItem(image: UIImage(named: "add"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(openAddBook))
        navigationItem.rightBarButtonItem = add
    }
    
    @objc func openAddBook() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BooksAddViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func loadBooks() {
        BookService.getBooks { (success, message, books) in
            if success! {
                self.books = books!
                self.tableView.reloadData()
                self.loader.stopAnimating()
            } else {
                let alert = UIAlertController(title: "Alert", message: "Message", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.loader.stopAnimating()
            }
        }
    }
    
    func deleteBook(book: Book, completion: @escaping (_ success: Bool?, _ message: String?) -> Void) {
        BookService.deleteBook(book: book) { (success, message) in
            if success! {
                self.tableView.reloadData()
            } else {
                let alert = UIAlertController(title: "Erro", message: message!, preferredStyle: UIAlertControllerStyle.alert)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

extension BooksViewController: UITableViewDelegate, UITableViewDataSource {
    func setup(tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.addSubview(refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return books.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.accessoryType = UITableViewCellAccessoryType.detailDisclosureButton
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let book = books[indexPath.row]
        cell.textLabel?.text = book.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BookViewController") as? BookViewController
        vc?.book = books[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alert = UIAlertController(title: "", message: "Deseja deletar?", preferredStyle: UIAlertControllerStyle.actionSheet)
            let defaultAction = UIAlertAction(title: "Sim",
                                              style: .default) { (action) in
            
                                                self.deleteBook(book: self.books[indexPath.row]) { (success, message) in
                                                    if success! {
                                                        self.books.remove(at: indexPath.row)
                                                        tableView.deleteRows(at: [indexPath], with: .fade)
                                                    }
                                                }
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            alert.addAction(defaultAction)
            alert.addAction(cancelAction)

            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

