//
//  BooksAddViewController.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit
import InputMask

class BooksAddViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, MaskedTextFieldDelegateListener {

    var maskedDelegate: MaskedTextFieldDelegate!

    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var isbnTF: UITextField!
    @IBOutlet weak var authorsPV: UIPickerView!
    
    private var authors: [Author] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPickerView(pickerView: authorsPV)
        customNavigationController()
        setupMaskData()
        loadAuthors()
    }
    
    func setupPickerView(pickerView: UIPickerView) {
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    func setupMaskData() {
        maskedDelegate = MaskedTextFieldDelegate(format: "[000]-[0]-[00]-[000000]-[0]")
        maskedDelegate.listener = self
        isbnTF.delegate = maskedDelegate
        isbnTF.placeholder = "XXX-X-XX-XXXXXX-X"
        titleTF.delegate = self
    }

    func customNavigationController() {
        self.title = "Novo Livro"
        let add = UIBarButtonItem(title: "Adicionar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(addBook))
        navigationItem.rightBarButtonItem = add
    }
    
    @objc func addBook() {
        BookService.addBook(title: titleTF.text!, isbn: isbnTF.text!, authorId: authorsPV.selectedRow(inComponent: 0)) { (success, message) in
            if success! {
                self.navigationController?.popViewController(animated: true)
            } else {
                let alert = UIAlertController(title: "Alert", message: message!, preferredStyle: UIAlertControllerStyle.alert)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func loadAuthors() {
        AuthorService.getAuthors { (success, message, authors) in
            if success! {
                self.authors = authors!
                self.authorsPV.reloadAllComponents()
            } else {
                let alert = UIAlertController(title: "Alert", message: "Message", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return authors.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(authors[row].firstName!) \(authors[row].lastName!)"
    }
}
