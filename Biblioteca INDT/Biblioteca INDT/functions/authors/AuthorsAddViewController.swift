//
//  AuthorsAddViewController.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class AuthorsAddViewController: UIViewController {
    
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavigationController()
    }
    
    func customNavigationController() {
        self.title = "Novo Autor"
        let add = UIBarButtonItem(title: "Adicionar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(addAuthor))
        navigationItem.rightBarButtonItem = add
    }
    
    @objc func addAuthor() {
        AuthorService.addAuthor(firstName: firstNameTF.text!, lastName: lastNameTF.text!) { (success, message) in
            if success! {
                self.navigationController?.popViewController(animated: true)
            } else {
                let alert = UIAlertController(title: "Alert", message: message!, preferredStyle: UIAlertControllerStyle.alert)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
