//
//  SecondViewController.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class AuthorsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    private var authors: [Author] = []
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(AuthorsViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.gray
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        loadAuthors()
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Autores"
        self.loader.hidesWhenStopped = true
        setup(tableView: tableView)
        customNavigationController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loader.startAnimating()
        loadAuthors()
    }
    
    func customNavigationController() {
        let add = UIBarButtonItem(image: UIImage(named: "add"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(openAddAuthor))
        navigationItem.rightBarButtonItem = add
    }
    
    @objc func openAddAuthor() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AuthorsAddViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func loadAuthors() {
        AuthorService.getAuthors { (success, message, authors) in
            if success! {
                self.authors = authors!
                self.tableView.reloadData()
                self.loader.stopAnimating()
            } else {
                let alert = UIAlertController(title: "Alert", message: message!, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.loader.stopAnimating()
            }
        }
    }
    
    func deleteAuthor(author: Author, completion: @escaping (_ success: Bool?, _ message: String?) -> Void) {
        AuthorService.deleteAuthor(author: author) { (success, message) in
            if success! {
                self.tableView.reloadData()
                self.loader.stopAnimating()
            } else {
                let alert = UIAlertController(title: "Erro", message: message!, preferredStyle: UIAlertControllerStyle.alert)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

extension AuthorsViewController: UITableViewDelegate, UITableViewDataSource {
    func setup(tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.addSubview(refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return authors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.accessoryType = UITableViewCellAccessoryType.detailDisclosureButton
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let author = authors[indexPath.row]
        cell.textLabel?.text = "\(author.firstName!) \(author.lastName!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AuthorViewController") as? AuthorViewController
        vc?.author = authors[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alert = UIAlertController(title: "", message: "Deseja deletar?", preferredStyle: UIAlertControllerStyle.actionSheet)
            let defaultAction = UIAlertAction(title: "Sim",
                                              style: .default) { (action) in
                                                
                                                self.loader.startAnimating()
                                                self.deleteAuthor(author: self.authors[indexPath.row]) { (success, message) in
                                                    if success! {
                                                        self.authors.remove(at: indexPath.row)
                                                        tableView.deleteRows(at: [indexPath], with: .fade)
                                                    }
                                                }
                                                
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            alert.addAction(defaultAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

