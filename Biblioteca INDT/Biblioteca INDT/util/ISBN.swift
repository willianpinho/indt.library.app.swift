//
//  ISBN.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

class ISBN {
    func calcCheckDigit( _ isbn:String ) -> Character {
        var result:Character = "?"
        let _isbn = isbn.removeWord( "-" ).removeWord( " " )
        
        switch _isbn.count {
            
        case 9:
            result = calcCheckDigitForISBN10( _isbn )
            break
        case 12:
            result = calcCheckDigitForISBN13( _isbn )
            break
            
        default:
            print( "ERROR" )
        }
        
        return result
    }
    
    func calcCheckDigitForISBN10( _ code:String ) -> Character {
        
        var result:Character = "?"
        
        var sum:Int = 0
        
        for ( index, char ) in code.enumerated() {
            
            let number = char.toInt()
            sum += number * ( index + 1 )
        }
        
        let checkDigit = 11 - ( 11 - sum % 11 ) % 11
        
        if checkDigit > 9 {
            
            result = "X"
        }
        else {
            
            result = Character( String( checkDigit ) )
        }
        
        return result
    }
    
    func calcCheckDigitForISBN13( _ code:String ) -> Character {
        
        var result:Character = "?"
        
        var sum:Int = 0
        var oddIndex:Bool = false
        
        for char in code {
            
            let number = char.toInt()
            sum += number * ( oddIndex ? 3 : 1 )
            oddIndex = !oddIndex
        }
        
        let checkDigit = ( 10 - sum % 10 ) % 10
        
        result = Character( String( checkDigit ) )
        
        return result
    }
}

extension Character {
    
    func toInt() -> Int {
        return Int( String( self ) )!
    }
}

extension String {
    func removeWord( _ word: String ) -> String {
        
        var result = ""
        
        let textCharArr = Array( self )
        let wordCharArr = Array( word )
        
        var possibleMatch = ""
        
        var i = 0, j = 0
        while i < textCharArr.count {
            
            if textCharArr[ i ] == wordCharArr[ j ] {
                
                if j == wordCharArr.count - 1 {
                    
                    possibleMatch = ""
                    j = 0
                }
                else {
                    
                    possibleMatch.append( textCharArr[ i ] )
                    j += 1
                }
            }
            else {
                result.append( possibleMatch )
                possibleMatch = ""
                
                if j == 0 {
                    
                    result.append( textCharArr[ i ] )
                }
                else {
                    
                    j = 0
                    i -= 1
                }
            }
            
            i += 1
        }
        
        return result
    }
}

