//
//  Stringfy.swift
//  Biblioteca INDT
//
//  Created by Willian Pinho on 03/05/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

class Stringfy {
    static func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
            options = JSONSerialization.WritingOptions.prettyPrinted
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: options)
            if let string = String(data: data, encoding: String.Encoding.utf8) {
                return string
            }
        } catch {
            print(error)
        }
        
        return ""
    }
}
